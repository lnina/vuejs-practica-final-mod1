import axios from 'axios'

export default 
{
  install: (Vue) => 
  {
    const Service = 
    {
      get (url) 
      {
        const init = 
        {
          method: 'GET',
          url
        }
        return axios(init)
      },

      transformar(paises)
      {
        let retornar=[];
        for(let p in paises)
        {
          const {name,capital,population,region,subregion,flag}=paises[p];
          retornar.push({
            name: name,
            capital: capital,
            population: population,
            region: region,
            subregion: subregion,
            flag: flag,
          });
        }
        return retornar;
      }

    }
    Vue.prototype.$service = Service;
  }
};