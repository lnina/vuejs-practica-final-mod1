import Vue from 'vue'
import App from './App.vue'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css' //Vuesax styles
import 'material-icons/iconfont/material-icons.css';

import Storage from './plugins/Storage';
import Service from './plugins/Service';


Vue.use(Vuesax, {
  // options here
});
Vue.use(Storage);
Vue.use(Service);


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
